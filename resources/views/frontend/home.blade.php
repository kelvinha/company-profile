@extends('layouts.frontend')
@section('content')
<!-- Start Slider Area -->
<div id="home" class="slider-area">
    <div class="bend niceties preview-2">
      <div id="ensign-nivoslider" class="slides">
        @foreach($banner as $item)
        <img src="{{ asset('files/content') }}/{{$item->filename}}" alt="" title="#slider-direction-{{ $item->id }}">
        @endforeach
      </div>
      
      <!-- direction 1 -->
      @foreach($banner as $item)
      <div id="slider-direction-{{ $item->id }}" class="slider-direction slider-one">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
                <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                  <h1 class="title2">{{ $item->nama }}</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  <!-- End Slider Area -->

  <!-- Start About area -->
  <div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>About abynet</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
              <a href="#">
								  <img src="{{ asset('vendor-frontend') }}/img/logo/12.jpg" alt="">
								</a>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
            <div class="single-well">
              <a href="#">
                <h4 class="">abynet</h4>
              </a>
              <p>
                Merupakan perusahaan yang bergerak di bidang Teknologi Informasi dan komunikasi dengan Spesialisasi di bidang General Trading, Computer & IT Consulting, Network & IT Solution, Hardware Services & Maintenance, Web Design & Development, Programming, Multimedia, Security System, Graphic Design & Printing, EO [Event Organizer]
              </p>
              <p>
                Dengan Didukung para tenaga ahli di bidang Teknologi Informasi dan Komunikasi kami telah menciptakan banyak produk-product yang inovatif.
              </p>
              <p>
                Kami akan terus memberikan pelayanan yang terbaik bagi seluruh mitra serta fokus pada misi dan misi kamu
              </p>
            
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End About area -->

  <div id="product" class="portfolio-area area-padding fix">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Portfolio</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Start Portfolio -page -->
      
        <div class="awesome-project-content">
          <!-- single-awesome-project start -->
          @foreach($portofolio as $item)
          <div class="col-md-3 col-sm-3 col-xs-12 design development">
            <div class="single-awesome-project">
              <div class="awesome-img">

                <a href=""><img src="{{ asset('files/content') }}/{{$item->filename}}" alt="" title="#slider-direction-1"></a>

                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="{{ asset('files/content') }}/{{$item->filename}}">
                      <h4>{{$item->nama}}</h4>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>


      <!-- start our client -->
<div class="area-padding">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
          <h2>Aur Partner</h2>
        </div>
      </div>
    </div>
  
    <div class="testimonials-area">
      <div class="testi-inner area-padding">
        <div class="testi-overly"></div>
        <div class="container ">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <!-- Start testimonials Start -->
              <div class="testimonial-content text-center">
                <div class="testimonial-carousel">
                @foreach($partner as $item)
                      <img src="{{ asset('files/content')}}/{{ $item->filename }}" style="width: 15%; text-align: center; margin-left: auto; margin-right: auto;">
                @endforeach
                  <!-- End single item -->
                </div>
              </div>
              <!-- End testimonials end -->
            </div>
            <!-- End Right Feature -->
          </div>
        </div>
      </div>
    </div>

  </div>

 <div class="area-padding">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="section-headline text-center">
          <h2>Aur Client</h2>
        </div>
      </div>
    </div>
    <div class="testimonials-area">
      <div class="testi-inner area-padding">
        <div class="testi-overly"></div>
        <div class="container ">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <!-- Start testimonials Start -->
              <div class="testimonial-content text-center">
                <div class="testimonial-carousel">
                @foreach($client as $item)
                    <img src="{{ asset('files/content') }}/{{$item->filename}}"  style="width: 15%; text-align: center; margin-left: auto; margin-right: auto;">
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
    <!-- end  our client -->
<div id="contact" class="contact-area">
  <div class="contact-inner area-padding">
    <div class="contact-overly"></div>
    <div class="container ">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Contact us</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Start contact icon column -->
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="contact-icon text-center">
            <div class="single-icon">
              <i class="fa fa-mobile"></i>
              <p>
                Call: +1 5589 55488 55<br>
                <span>Monday-Friday (9am-5pm)</span>
              </p>
            </div>
          </div>
        </div>
        <!-- Start contact icon column -->
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="contact-icon text-center">
            <div class="single-icon">
              <i class="fa fa-envelope-o"></i>
              <p>
                Email: info@example.com<br>
                <span>Web: www.example.com</span>
              </p>
            </div>
          </div>
        </div>
        <!-- Start contact icon column -->
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="contact-icon text-center">
            <div class="single-icon">
              <i class="fa fa-map-marker"></i>
              <p>
                Location: A108 Adam Street<br>
                <span>NY 535022, USA</span>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">

        <!-- Start Google Map -->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <!-- Start Map -->
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13677.674575737014!2d106.27385416350607!3d-6.371901929187137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e4211395de6f8cb%3A0x76b1a169b054eb43!2sCurug%20Mulya!5e0!3m2!1sid!2sid!4v1668681593007!5m2!1sid!2sid" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
          <!-- End Map -->
        </div>
        <!-- End Google Map -->

        <!-- Start  contact -->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form contact-form">
            @if ($m = Session::get('msg'))
              <div id="sendmessage">{{ $m }}</div>
            @endif
            @if ($m = Session::get('err'))
              <div id="errormessage">{{ $m }}</div>
            @endif
            <form action="{{ route('messages.store') }}" method="post">
              @csrf
              <div class="form-group">
                <input type="text" name="name" class="form-control" required id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" required name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" required name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" required name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>
        </div>
        <!-- End Left contact -->
      </div>
    </div>
  </div>
</div>
@endsection