@extends('layouts.frontend')
@section('content')
  <!-- Start Bottom Header -->
  <div class="header-bg page-area">
    <div class="home-overly"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content text-center">
            <div class="header-bottom">
              <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                <h1 class="title2">Layanan</h1>
              </div>
              <div class="layer3 wow zoomInUp" data-wow-duration="2s" data-wow-delay="1s">
                <h2 class="title3">Kompentensi</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Layanan dan Kompentensi</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
              <a href="#">
								  <img src="{{ asset('vendor-frontend') }}/img/logo/12.jpg" alt="">
								</a>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
            <div class="single-well">
              <a href="#">
                <h4 class="">abynet</h4>
              </a>
              <p>
                  Sudah menjadi tekad kami untuk selalu, melalui pegembangan keahlian dan profesionalisme SDM, serta melakukan
                  inovasi serta solusi untuk mendukung tercapainya visi untuk menjadi yang terdepan di bidang jasa teknologi informasi.
                  Untuk itu dalam perkembangan usahanya, kami menitikberatkan pada beberapa kompetensi (Core Business) yang saling mendukung
                  di bidang layanan teknologi informasi, yaitu:
                </p>
                <ul>
                    <li>
                      <i class="fa fa-check"></i> IT Konsultan
                    </li>
                    <li>
                      <i class="fa fa-check"></i> Infrastructure Solutions & Network Service
                    </li>
                    <li>
                      <i class="fa fa-check"></i> content Provider
                    </li>
                    <li>
                      <i class="fa fa-check"></i> System Operation & Maintenance
                    </li>
                    <li>
                      <i class="fa fa-check"></i> Software Development
                    </li>
                    <li>
                        <i class="fa fa-check"></i> Multimedia
                    </li>
                    <li>
                        <i class="fa fa-check"></i> Event Organizer [EO]
                      </li>
                  </ul>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  @endsection