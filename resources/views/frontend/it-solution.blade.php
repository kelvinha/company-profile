@extends('layouts.frontend')
@section('content')
  <!-- Start Bottom Header -->
  <div class="header-bg page-area">
    <div class="home-overly"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content text-center">
            <div class="header-bottom">
              <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                <h1 class="title2">IT Solution</h1>
              </div>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Section 1 -->

  <div class="faq-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Product Description</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="faq-details">
            <div class="panel-group" id="accordion">
              <!-- Panel Default -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="check-title">
											<a data-toggle="collapse" class="active" data-parent="#accordion" href="#check1">
                                                <span class="acc-icons"></span>IT Konsultan
											</a>
										</h4>
                </div>
                <div id="check1" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <p>
                      Kami Menyediakan layanan di bidang konsultasi IT secara profesional untuk:
                    </p>
                    <ul>
                        <li>
                            <i class="fa fa-check"></i> Membantu anda dalam mengidentifikasi permasalahan dan menjawab perkembangan IT yang multi disiplin.
                        </li>
                        <li>
                            <i class="fa fa-check"></i> Menyusun kerangka acuan (rencana induk) bagi pegembangan TI secara sistematis, bertahap dan berkesinambungan agar sinergi terhadap siklus program kerja masing-masing fungsi unit kerja.
                        </li>
                        <li>
                            <i class="fa fa-check"></i> Membantu anda merumuskan arsitektur sistem data, dan mencakup aspek bisni, aplikasi data, dan teknologi serta kerangka kerja pengelolaannya agar dapat menjamin interkoneksi dan interoperablitas berbagai infrastruktur yang dibangun.
                        </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- End Panel Default -->
              <!-- Panel Default -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="check-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#check2">
                                                <span class="acc-icons"></span>Infrastructure Solution & Networs Services
											</a>
										</h4>
                </div>
                <div id="check2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>
                      Layanan pegembangan infrastruktur dan jaringan teknologi informasi. Dalam layanan ini CV.AbyNET bekerjasama dengan berbabgai patner penyedia perangkat dan jaringan untuk menyediakan total solusi kepada klien.
                    </p>
                  </div>
                </div>
              </div>
              <!-- End Panel Default -->
              <!-- Panel Default -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="check-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#check3">
                                                <span class="acc-icons"></span>Content Provider
											</a>
										</h4>
                </div>
                <div id="check3" class="panel-collapse collapse ">
                  <div class="panel-body">
                    <p>
                      Kami berorientasi pada pegembangan dan peyediaan content/informasi  yang interaktif melalui web dan mobile techonlogy, sepert: Value Addes Service SMS (VAS SMS), SMS Gateway, Multimedia Profile dll.
                    </p>
                  </div>
                </div>
              </div>
              <!-- End Panel Default -->
                  <!-- Panel Default -->
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="check-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#check4">
                                                    <span class="acc-icons"></span>System Operation & Maintenance
                                                </a>
                                            </h4>
                    </div>
                    <div id="check4" class="panel-collapse collapse ">
                      <div class="panel-body">
                        <p>
                          Kami Siap membantu anda dalam mengelola sistem informasi, sehingga anda dapat lebih berkonsentrasi dalam menjalankan bisnis anda, untuk menghemat dalam peyediaan resource untuk mengikuti perkembangan teknologi informasi yang tentunya akan menyita waktu dan tenaga dalam perjalanan bisnis anda.
                        </p>
                      </div>
                    </div>
                  </div>
                  <!-- End Panel Default -->
                   <!-- Panel Default -->
                   <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="check-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#check5">
                                                    <span class="acc-icons"></span>Software Development
                                                </a>
                                            </h4>
                    </div>
                    <div id="check5" class="panel-collapse collapse ">
                      <div class="panel-body">
                        <p>
                          Kami selalu mencari dan menawarkan solusi di bidang riset dan pegembangan software secara customize, termasuk di dalam lingkup tersebut adalah jasa perancangan, pegembanfan aplikasi, hingga implementasi dan pelatihan operasionalnya. Kemampuan, penguasan teknologi, karena pegalaman (portfolio) kami di bidang software development cukup handal dan selalu mengikuti perkembangan teknologi terkini.
                        </p>
                      </div>
                    </div>
                  </div>
                  <!-- End Panel Default -->
                    <!-- Panel Default -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="check-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#check6">
                                                        <span class="acc-icons"></span>Multimedia
                                                    </a>
                                                </h4>
                        </div>
                        <div id="check6" class="panel-collapse collapse ">
                          <div class="panel-body">
                            <p>
                              Kami menyediakan juga jasa sewa alat multimedia, editing Audio Visual, pembuatan konten pembelajaran, Compro dll.
                            </p>
                          </div>
                        </div>
                      </div>
                      <!-- End Panel Default -->
            </div>
          </div>
        </div>
      </div>
      <!-- end Row -->
    </div>
  </div>

  <!-- Section 2 -->

  <div class="faq-area area-padding">
    <div class="container">
      <div id="about" class="about-area area-padding">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="section-headline text-center">
                <h2>Enterprise product</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <!-- single-well start-->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="well-left">
                <div class="single-well">
                  <a href="#">
                                      <img src="{{ asset('vendor-frontend') }}/img/logo/12.jpg" alt="">
                                    </a>
                </div>
              </div>
            </div>
            <!-- single-well end-->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="well-middle">
                <div class="single-well">
                  <a href="#">
                    <h4 class="">abynet</h4>
                  </a>
                  <p>
                    perkembangan teknologi informasi dan telekomunikasi telah mengubah pola dan perilaku bisnis baik individu maupun korporasi. Tingkat persaingan bisnis juga semakin ketat. Kenyataan ini telah mendorong para pelaku bisnis untuk menggunakan strategi dan teknologi yang efektif, efisien untuk meningkatkan keuntungan dan memenangkan persaingan. Kondisi di atas secara langsung telah menciptakan pasar bagi industri dan provider teknologi di bidang informasi dan komunikasi
                  </p> 
                  <p>
                      TIdak heran jika muncul banyak perusahaan yang menawarkan berbagai produk dan solusinya. Namun CV. ABYNET sebagai salah satu entitas bisnis di bidang teknologi informasi dan komunikasi memberikan suatu solusi yang lebih dari yang lain untuk kepuasan klien.
                  </p>               
                </div>
              </div>
            </div>
            <!-- End col-->
          </div>
          <div class="row" style="padding-top: 15px;">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="tab-menu">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li class="active">
                    <a href="#p-view-1" role="tab" data-toggle="tab">Cognos</a>
                  </li>
                  <li>
                    <a href="#p-view-2" role="tab" data-toggle="tab">pentaho</a>
                  </li>
                  <li>
                    <a href="#p-view-3" role="tab" data-toggle="tab">Oracle</a>
                  </li>
                  <li>
                    <a href="#p-view-4" role="tab" data-toggle="tab">Asterisk</a>
                  </li>
                  <li>
                    <a href="#p-view-5" role="tab" data-toggle="tab">IBM</a>
                  </li>
                </ul>
              </div>
              <div class="tab-content">
                <div class="tab-pane active" id="p-view-1">
                  <div class="tab-inner">
                    <div class="event-content head-team">
                      <h4>COGNOS</h4>
                      <p>
                       Solusi business intelegence berbasiskan web dengan integrated report, analiysis, scorecarding dan event management.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="p-view-2">
                  <div class="tab-inner">
                    <div class="event-content head-team">
                      <h4>Pentaho</h4>
                      <p>
                        salah satu tools untuk bussiness intelegence yang bersifat open source, berbasis web. (Biseerver, Data Integration, Analysis, Reporting, Data Mining).
                      </p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="p-view-3">
                  <div class="tab-inner">
                    <div class="event-content head-team">
                      <h4>Oracle</h4>
                      <p>
                        Oracle : Database Relasional yang terdiri dari kumpulan data dalam suaru sistem manajemen basis data RDBMS (Relational Data Base Management System) yang multi-platform.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="p-view-4">
                    <div class="tab-inner">
                      <div class="event-content head-team">
                        <h4>Asterisk</h4>
                        <p>
                          Menghubungkan segala jenis telepon, IP-Telepon, VIOP Interface ke Interface lainnya.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="p-view-5">
                    <div class="tab-inner">
                      <div class="event-content head-team">
                        <h4>IBM</h4>
                        <p>
                          Memberi solusi kepada ETL Developer untuk membangun proses ETL ini dengan mudah.
                        </p>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
      </div>
      <!-- end Row -->
    </div>
  </div>

<!-- Section 3 -->
  
<div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>System Support</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
              <a href="#">
								  <img src="{{ asset('vendor-frontend') }}/img/blog/4.jpg" alt="">
								</a>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
            <div class="single-well">
              <a href="#">
                <h4 class="">Product Guarantee</h4>
              </a>
              <p>
              CV. ABYNET akan memberikan garansi product sesuai dengan kesepakatan dengan rekanan yang meliputi:
              </p>
              <ul>
                <li>
                    <i class="fa fa-check"></i> Perfomansi
                </li>
                <li>
                    <i class="fa fa-check"></i> Akurasi
                </li>
                <li>
                    <i class="fa fa-check"></i> Bug Fixing  
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
      <div class="row" style="padding-top: 25px;">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="well-middle">
              <div class="single-well">
                <a href="#">
                  <h4 class="">Product Guarantee</h4>
                </a>
                <p>
                CV. ABYNET akan memberikan garansi product sesuai dengan kesepakatan dengan rekanan yang meliputi:
                </p>
                <ul>
                  <li>
                      <i class="fa fa-check"></i> Perfomansi
                  </li>
                  <li>
                      <i class="fa fa-check"></i> Akurasi
                  </li>
                  <li>
                      <i class="fa fa-check"></i> Bug Fixing  
                  </li>
                </ul>
              </div>
            </div>
          </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="well-left">
              <div class="single-well">
                <a href="#">
                <img src="{{ asset('vendor-frontend') }}/img/blog/3.jpg" alt="">
                </a>
              </div>
            </div>
          </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  @endsection