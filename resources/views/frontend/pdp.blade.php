@extends('layouts.frontend')
@section('content')
  <!-- Start Bottom Header -->
  <div class="header-bg page-area">
    <div class="home-overly"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content text-center">
            <div class="header-bottom">
              <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                <h1 class="title2">Detail Product</h1>
              </div>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Section 1  -->
<div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row" style="padding-right: 50px; padding-left: 50px;">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
                <div id="home" class="slider-area">
                    <div class="bend niceties preview-2">
                      <div id="ensign-nivoslider" class="slides" style="border-radius: 20px;">
                        <img src="{{ asset('vendor-frontend') }}/img/slider/slider1.jpg" alt=""/>
                        <img src="{{ asset('vendor-frontend') }}/img/slider/slider2.jpg" alt=""/>
                        <img src="{{ asset('vendor-frontend') }}/img/slider/slider3.jpg" alt=""/>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
          <h3> <strong> Banner Indoor</strong></h3>
            <div class="single-well">
                <span class="saleon">Rp. 500.000</span> 
                <hr>
                <strong>Detail Product</strong>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam sit dignissimos pariatur amet unde est ab, odio nihil cumque quae facilis dolor esse commodi libero ad magnam officia corrupti tempore!
              </p>

            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  @endsection