@extends('layouts.frontend')
@section('content')
  <!-- Start Bottom Header -->
  <div class="header-bg page-area">
    <div class="home-overly"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content text-center">
            <div class="header-bottom">
              <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                <h1 class="title2">Price List</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="portfolio" class="portfolio-area area-padding fix">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Price List</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Start Portfolio -page -->
        <div class="awesome-project-content">
          <!-- single-awesome-project start -->
          <div class="col-md-6 col-sm-6 col-xs-6 design development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="{{ asset('vendor-frontend') }}/img/blog/7.jpg" alt="" style="width: 100%;"/></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a href="https://fontawesome.com/v4/icon/download" target="_blank">
                      <h4>Digital Printing</h4>
                      <span><i class="fa fa-download"></i> Download</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <div class="col-md-6 col-sm-6 col-xs-6 design development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="{{ asset('vendor-frontend') }}/img/blog/8.jpg" alt="" style="width: 100%;"/></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a href="https://fontawesome.com/v4/icon/download" target="_blank">
                      <h4>Banner In Doorr</h4>
                      <span><i class="fa fa-download"></i> Download</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @endsection