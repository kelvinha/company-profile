@extends('layouts.frontend')
@section('content')
  <!-- Start Bottom Header -->
  <div class="header-bg page-area">
    <div class="home-overly"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content text-center">
            <div class="header-bottom">
              <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                <h1 class="title2">Visi dan Misi</h1>
              </div>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Visi dan Misi</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
              <a href="#">
								  <img src="{{ asset('vendor-frontend') }}/img/logo/12.jpg" alt="">
								</a>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
            <div class="single-well">
              <a href="#">
                <h4 class="">abynet</h4>
              </a>
              <p>
                  Sudah menjadi tekad kami untuk selalu, melalui pegembangan keahlian dan profesionalisme SDM, serta melakukan
                  inovasi serta solusi untuk mendukung tercapainya visi untuk menjadi yang terdepan di bidang jasa teknologi informasi.
                  Untuk itu dalam perkembangan usahanya, kami menitikberatkan pada beberapa kompetensi (Core Business) yang saling mendukung
                  di bidang layanan teknologi informasi, yaitu:
                </p>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>

  <div class="wellcome-area">
    <div class="well-bg">
      <div class="test-overly"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="wellcome-text">
              <div class="well-text text-center">
                <h2>Visi</h2>
                <p>
                  Mengutamakan kualitas demi terciptanya layanan terbaik serta kepuasan konsumen dimana visi tersebut berkembang menjadi beberapa misi akomodatif. Kami memberikan suatu produk dan layanan yang berkualitas tinggi sehingga dapat meningkatkan penjualan serta brand image dari produk layanan kami.
                </p>
               
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="wellcome-text">
              <div class="well-text text-center">
                <h2>Misi</h2>
                <p>
                  Menerapkan perpaduan antara pemakaian teknologi tingggi dengan SDM yang professional dan berkompeten di bidangnya. Menyiapkan perspektif yang dinamis untuk mendukung mitra guna menapaki seluruh peluang-peluang bisnis di masa kini dan masa mendatang demi meraih kesukseksan.
                </p>
             
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection