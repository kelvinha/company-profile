<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
        <div class="sidebar-brand-text mx-3">Comp Pro</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Route::is('dashboard') ? 'active' : ''}}">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Content Comp Pro
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ Route::is('kelola.content') ? 'active' : ''}}">
        <a class="nav-link collapsed" href="{{ route('kelola.content') }}"
            aria-expanded="true">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kelola Konten</span>
        </a>
    </li>
    <li class="nav-item {{ Request::is('admin/produk/*') ? "active" : "" }}">
        <a class="nav-link {{ Request::is('admin/produk/*') ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#contentProduk"
        aria-expanded="true" aria-controls="contentProduk">
        <i class="fas fa-archive"></i>
        <span>Konten Produk</span>
        </a>
        <div id="contentProduk" class="collapse {{ Request::is('admin/produk/*') ? "show" : "" }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">List</h6>
                <a class="collapse-item {{ Route::is('kelola.produk') ? "active" : "" }}" href="{{ route('kelola.produk') }}">Kelola Produk</a>
                <a class="collapse-item {{ Route::is('kategori.produk') ? "active" : "" }}" href="{{ route('kategori.produk') }}">Kategori Produk</a>
            </div>
        </div>
    </li>
    <li class="nav-item {{ Route::is('messages') ? "active" : "" }}">
        <a class="nav-link collapsed" href="{{ route('messages') }}"
            aria-expanded="true">
            <i class="fas fa-envelope-open-text"></i>
            <span>Messages</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

</ul>
<!-- End of Sidebar -->