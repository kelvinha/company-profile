
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor')}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset('vendor')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var msg = button.data('msg')
        var sub = button.data('sub')
        var modal = $(this)
        modal.find('.modal-header #exampleModalLabel').text(sub)
        modal.find('.modal-body').html(msg)
        })
      </script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor')}}/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('vendor')}}/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('vendor')}}/vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('vendor')}}/js/demo/chart-area-demo.js"></script>
    <script src="{{ asset('vendor')}}/js/demo/chart-pie-demo.js"></script>
    <!-- Page level plugins -->
    <script src="{{ asset('vendor')}}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('vendor')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('vendor')}}/js/demo/datatables-demo.js"></script>

</body>

</html>