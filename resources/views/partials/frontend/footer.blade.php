
  <!-- Start Footer bottom Area -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <div class="footer-logo">
                  <img src="{{ asset('vendor-frontend') }}/img/logo/logo.png" alt="" width="15%" />
                </div>

                <p>Merupakan perusahaan yang bergerak di bidang Teknologi Informasi dan komunikasi.</p>
                <div class="footer-icons">
                  <ul>
                  <li>
                      <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-google"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-pinterest"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- end single footer -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <h4>information</h4>
                <p>
                  Jl. Jend. Sudirman KM.5 Kp. Curug Narimbang Mulia Rangkasbitung Lebak Ruko Blessing Cluster No.09
                </p>
                <div class="footer-contacts">
                  <p><span>Wa:</span> +62 838 78376611</p>
                  <p><span>Email:</span> contact@example.com</p>
                  <p><span>Working Hours:</span> 9am-5pm</p>
                </div>
              </div>
            </div>
          </div>
          <!-- end single footer -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <h4>Instagram</h4>
                <div class="flicker-img">
                  <a href="#"><img src="{{ asset('vendor-frontend') }}/img/portfolio/1.jpg" alt=""></a>
                  <a href="#"><img src="{{ asset('vendor-frontend') }}/img/portfolio/2.jpg" alt=""></a>
                  <a href="#"><img src="{{ asset('vendor-frontend') }}/img/portfolio/3.jpg" alt=""></a>
                  <a href="#"><img src="{{ asset('vendor-frontend') }}/img/portfolio/4.jpg" alt=""></a>
                  <a href="#"><img src="{{ asset('vendor-frontend') }}/img/portfolio/5.jpg" alt=""></a>
                  <a href="#"><img src="{{ asset('vendor-frontend') }}/img/portfolio/6.jpg" alt=""></a> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>AbyNet</strong>. All Rights Reserved
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <a href="https://wa.me/+6283878376611?text=Hallo+selamat+datang+di+abynet" class="back-to-top"><i class="fa fa-whatsapp"></i></a>


  <!-- JavaScript Libraries -->
  <script src="{{ asset('vendor-frontend') }}/lib/jquery/jquery.min.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/venobox/venobox.min.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/knob/jquery.knob.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/wow/wow.min.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/parallax/parallax.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/easing/easing.min.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/appear/jquery.appear.js"></script>
  <script src="{{ asset('vendor-frontend') }}/lib/isotope/isotope.pkgd.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('vendor-frontend') }}/contactform/contactform.js"></script>

  <script src="{{ asset('vendor-frontend') }}/js/main.js"></script>
</body>

</html>
