 <!-- Navigation -->
 <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <img src="{{ asset('vendor-frontend') }}/img/logo/logo.png" alt="" width="12%" style="margin-top: auto; margin-buttom: auto;"  />
  
    <!-- Brand -->
    <a class="navbar-brand page-scroll sticky-logo" href="{{ route('index') }}">
    <!-- <h1><span>Aby</span>Net</h1> -->
    </a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
    <ul class="nav navbar-nav navbar-right">
        <li class="active">
        <a class="page-scroll" href="{{ route('index') }}">Home</a>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Our Product<span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{route('index.itsolution') }}" >IT Solution</a></li>
            <li><a href="{{ route('index.produk') }}">Digital Printing</a></li>
        </ul> 
        </li>
        <li>
        <a class="page-scroll" href="{{ route('index.price.list') }}">Price List</a>
        </li>
        <li>
        <a class="page-scroll" href="{{route('index.visimisi')}}">Visi misi</a>
        </li>
        <li>
        <a class="page-scroll" href="{{route('index.layanan')}}">Layanan & Kompentensi</a>
        </li>
    
    </ul>
    </div>
    <!-- navbar-collapse -->
</nav>
<!-- END: Navigation -->