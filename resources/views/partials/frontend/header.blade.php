<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Abynet</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('vendor-frontend') }}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('vendor-frontend') }}/lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
  <link href="{{ asset('vendor-frontend') }}/lib/owlcarousel/owl.carousel.css" rel="stylesheet">
  <link href="{{ asset('vendor-frontend') }}/lib/owlcarousel/owl.transitions.css" rel="stylesheet">
  <link href="{{ asset('vendor-frontend') }}/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="{{ asset('vendor-frontend') }}/lib/animate/animate.min.css" rel="stylesheet">
  <link href="{{ asset('vendor-frontend') }}/lib/venobox/venobox.css" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="{{ asset('vendor-frontend') }}/css/nivo-slider-theme.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('vendor-frontend') }}/css/style.css" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="{{ asset('vendor-frontend') }}/css/responsive.css" rel="stylesheet">
</head>