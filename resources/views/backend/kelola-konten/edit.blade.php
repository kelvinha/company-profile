@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    @if ($m = Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
        <strong>Error!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    <div class="card">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Edit Konten</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <form class="user" method="POST" action="{{ route('kelola.content.update', $konten->id ) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-8">
                                <div class="form-group mb-2">
                                    <div class="col-sm-12">
                                        <input type="text" name="nama" value="{{ $konten->nama }}" class="form-control" placeholder="Nama Content" autocomplete="off">
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <h4>Konten Saat ini</h4>
                                    <img src="{{ asset('files/content') }}/{{ $konten->filename }}" class="img-fluid" width="50%" alt="">
                                </div>
                                <hr>
                                <div class="d-flex justify-content-left">
                                    <h5 class="mt-2">Ganti Konten: </h5>
                                    <div class="form-group mt-2">
                                        <div class="col-sm-12">
                                            <input type="file" name="file" accept=".jpg,.jpeg,.png" placeholder="File">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <p>Kategori Konten: </p>
                                    <div class="custom-control custom-checkbox small">
                                        <input type="radio" name="type" value="banner" {{ $konten->type == "banner" ? "checked" : "" }} class="custom-control-input" id="radio1">
                                        <label class="custom-control-label h5" for="radio1">Banner</label>
                                    </div>
                                    <div class="custom-control custom-checkbox small">
                                        <input type="radio" name="type" value="portfolio" {{ $konten->type == "portfolio" ? "checked" : "" }} class="custom-control-input" id="radio2">
                                        <label class="custom-control-label h5" for="radio2">Portfolio</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" align="center"> 
                            <button type="submit" class="btn btn-info">Perbarui</button>
                            <button onclick="window.history.back()" class="btn btn-danger">Kembali</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection