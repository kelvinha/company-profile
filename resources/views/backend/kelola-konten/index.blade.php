@extends('layouts.backend')
@section('content')
<div class="container-fluid" >
    @if ($m = Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
        <strong>Error!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    @if ($m = Session::get('msg'))
    <div class="alert alert-info alert-dismissible fade show p-2" role="alert">
        <strong>Selamat!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    <div class="card">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Tambah Konten</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    {{-- <div class="p-0"> --}}
                        <hr>
                        <form class="user" method="POST" action="{{ route('kelola.content.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" name="nama" required class="form-control" placeholder="Nama Content" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="file" name="file" required accept=".jpg,.jpeg,.png" placeholder="File">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <p>Kategori Konten: </p>
                                        <div class="custom-control custom-checkbox small">
                                            <input type="radio" name="type" required value="banner" class="custom-control-input" id="radio1">
                                            <label class="custom-control-label h5" for="radio1">Banner</label>
                                        </div>
                                        <div class="custom-control custom-checkbox small">
                                            <input type="radio" name="type" required value="portfolio" class="custom-control-input" id="radio2">
                                            <label class="custom-control-label h5" for="radio2">Portfolio</label>
                                        </div>
                                        <div class="custom-control custom-checkbox small">
                                            <input type="radio" name="type" required value="client" class="custom-control-input" id="radio3">
                                            <label class="custom-control-label h5" for="radio3">Our client</label>
                                        </div>
                                        <div class="custom-control custom-checkbox small">
                                            <input type="radio" name="type" required value="partner" class="custom-control-input" id="radio4">
                                            <label class="custom-control-label h5" for="radio4">Our Partner</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group" align="center"> 
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </form>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="mt-2"></div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">List Content</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead align="center">
                        <tr>
                            <th>Name</th>
                            <th>Kategori</th>
                            <th>Konten</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        @if ($kontens->count() == 0)
                            <tr>
                                <td colspan="7">Konten tidak tersedia</td>
                            </tr>
                        @endif
                        @foreach ($kontens as $item)
                            <tr class="text-nowrap">
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->type }}</td>
                                <td width="20%">
                                    <img src="{{ asset('files/content') }}/{{ $item->filename }}" width="35%" alt="">
                                </td>
                                <td>{{ tanggalIndonesia($item->created_at) }}</td>
                                <td>{{ tanggalIndonesia($item->updated_at) }}</td>
                                <td>
                                    <div class="row">
                                        <a href="{{ route('kelola.content.edit', $item->id) }}" class="btn btn-primary ml-2 shadow">Edit</a>
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('kelola.content.delete', $item->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn ml-2 btn-danger shadow">Delete</button>
                                    </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ml-1 mt-1 mb-1 d-flex justify-content-center">
                {{ $kontens->appends(request()->query())->links('vendor.pagination.bootstrap-4')  }}
            </div>
        </div>
    </div>
</div>
@endsection