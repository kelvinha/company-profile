@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    @if ($m = Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
        <strong>Error!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    @if ($m = Session::get('msg'))
    <div class="alert alert-info alert-dismissible fade show p-2" role="alert">
        <strong>Selamat!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    {{-- <div class="card">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Messages</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <form class="user" method="POST" action="{{ route('messages.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="name" required class="form-control" placeholder="Nama" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="email" name="email" required class="form-control" placeholder="email" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="subject" required class="form-control" placeholder="subject" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="message" required class="form-control" placeholder="messages" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" align="center"> 
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-2"></div> --}}
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Daftar Pesan</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead align="center">
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Action</th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                         @if ($messages->count() == 0)
                            <tr>
                                <td colspan="4">Messages tidak tersedia</td>
                            </tr>
                        @endif
                        @foreach ($messages as $item)
                            <tr class="text-nowrap">
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td class="text-wrap"> 
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-sub="{{ $item->subject }}" data-msg="{{ $item->message }}" data-target="#exampleModal">
                                        Show Messages
                                      </button>
                                </td>
                                <td>{{ tanggalIndonesia($item->created_at) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
  
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-info" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection