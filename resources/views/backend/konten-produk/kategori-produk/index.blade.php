@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    @if ($m = Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
        <strong>Error!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    @if ($m = Session::get('msg'))
    <div class="alert alert-info alert-dismissible fade show p-2" role="alert">
        <strong>Selamat!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    <div class="card">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Tambah Kategori</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <form class="user" method="POST" action="{{ route('kategori.produk.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="nama_kategori" required class="form-control" placeholder="Nama Kategori" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" align="center"> 
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-2"></div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">List Kategori</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead align="center">
                        <tr>
                            <th>Nama Kategori</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                         @if ($kategori_produk->count() == 0)
                            <tr>
                                <td colspan="7">Konten tidak tersedia</td>
                            </tr>
                        @endif
                        @foreach ($kategori_produk as $item)
                            <tr class="text-nowrap">
                                <td>{{ $item->nama_kategori }}</td>
                                <td>{{ tanggalIndonesia($item->created_at) }}</td>
                                <td>{{ tanggalIndonesia($item->updated_at) }}</td>
                                <td>
                                    <div class="row">
                                        <a href="{{ route('kategori.produk.edit', $item->id) }}" class="btn btn-primary ml-2 shadow">Edit</a>
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('kategori.produk.delete', $item->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn ml-2 btn-danger shadow">Delete</button>
                                    </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ml-1 mt-1 mb-1 d-flex justify-content-center">
                {{ $kategori_produk->appends(request()->query())->links('vendor.pagination.bootstrap-4')  }}
            </div>
        </div>
    </div>
</div>
@endsection