@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    @if ($m = Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
        <strong>Error!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    <div class="card">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Edit Kategori</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <form class="user" method="POST" action="{{ route('kategori.produk.update', $kategori_produk->id) }}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="nama_kategori" value="{{ $kategori_produk->nama_kategori }}" class="form-control" placeholder="Nama Kategori" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" align="center"> 
                            <button type="submit" class="btn btn-info">Perbarui</button>
                            <button onclick="window.history.back()" class="btn btn-danger">Kembali</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection