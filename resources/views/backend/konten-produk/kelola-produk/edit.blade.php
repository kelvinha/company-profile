@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    @if ($m = Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
        <strong>Error!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    <div class="card">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Edit Produk</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <form class="user" method="POST" action="{{ route('kelola.produk.update', $produk->id ) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group mb-2">
                                    <div class="col-sm-12">
                                        <label class="form-label">Produk: </label>
                                        <input type="text" name="nama" value="{{ $produk->nama }}" class="form-control" placeholder="Nama Content" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group mb-2">
                                    <div class="col-sm-12">
                                        <label class="form-label">Harga: </label>
                                        <input type="text" name="harga" value="{{ $produk->harga }}" class="form-control" placeholder="Nama Content" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="form-label">Kategori produk: </label>
                                        <select class="form-control" name="kategori_id">
                                            @foreach ($kategori_produk as $item)
                                                <option value="{{ $item->id }}" {{ $item->id == $produk->kategori_id ? "selected" : "" }}>{{ $item->nama_kategori }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div>
                                    <h4>Produk Saat ini</h4>
                                    <img src="{{ asset('files/produk') }}/{{ $produk->filename }}" class="img-fluid" width="50%" alt="">
                                </div>
                                <hr>
                                <div class="d-flex justify-content-left">
                                    <h5 class="mt-2">Ganti Produk: </h5>
                                    <div class="form-group mt-2">
                                        <div class="col-sm-12">
                                            <input type="file" name="file_produk" accept=".jpg,.jpeg,.png" placeholder="File">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" align="center"> 
                            <button type="submit" class="btn btn-info">Perbarui</button>
                            <button onclick="window.history.back()" class="btn btn-danger">Kembali</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection