@extends('layouts.backend')
@section('content')
<div class="container-fluid">
    @if ($m = Session::get('err'))
    <div class="alert alert-danger alert-dismissible fade show p-2" role="alert">
        <strong>Error!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    @if ($m = Session::get('msg'))
    <div class="alert alert-info alert-dismissible fade show p-2" role="alert">
        <strong>Selamat!</strong> {{ $m }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
    @endif
    <div class="card">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Tambah Produk</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                    <form class="user" method="POST" action="{{ route('kelola.produk.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="nama" required class="form-control" placeholder="Nama Produk" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" name="harga" required class="form-control" placeholder="Harga" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="form-label">Upload File Produk: </label>
                                        <input type="file" name="file_produk" accept=".jpg,.jpeg,.png" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select class="form-control" name="kategori_id">
                                            <option disabled selected>- Pilih Kategori -</option>
                                            @foreach ($kategori_produk as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama_kategori }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group" align="center"> 
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-2"></div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">List Produk</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead align="center">
                        <tr>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Produk</th>
                            <th>Kategori</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                         @if ($produk->count() == 0)
                            <tr>
                                <td colspan="7">Konten tidak tersedia</td>
                            </tr>
                        @endif
                        @foreach ($produk as $item)
                            <tr class="text-nowrap">
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->harga }}</td>
                                <td width="20%">
                                    <img src="{{ asset('files/produk') }}/{{ $item->filename }}" width="35%" alt="">
                                </td>
                                <td>{{ $item->getKategori->nama_kategori }}</td>
                                <td>{{ tanggalIndonesia($item->created_at) }}</td>
                                <td>{{ tanggalIndonesia($item->updated_at) }}</td>
                                <td>
                                    <div class="row">
                                        <a href="{{ route('kelola.produk.edit', $item->id) }}" class="btn btn-primary ml-2 shadow">Edit</a>
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('kelola.produk.delete', $item->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn ml-2 btn-danger shadow">Delete</button>
                                    </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="ml-1 mt-1 mb-1 d-flex justify-content-center">
                    {{ $produk->appends(request()->query())->links('vendor.pagination.bootstrap-4')  }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection