<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::DASHBOARD;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) 
    {
        try {
            $validator = Validator::make($request->all(), [
                'username' => ['required','string'],
                'password' => ['required','string','min:8'],
            ]);
    
            if ($validator->fails()){
                Session::flash('error',$validator->messages()->first());
                $request->flash();
                return back()->withErrors($validator);
            }
    
            $data = User::where('username',$request->username)->first();
            if ($data) {
                if (Hash::check($request->password, $data->password)) {
                    Session::put('user', $data);
                    Session::put('login', true);
                    return redirect()->route('dashboard');
                } else {
                    Session::flash('err',"Username / Password salah");
                    return back();
                }
            } else {
                Session::flash('err',"User tidak tersedia");
                return back();
            }
        } catch (\Throwable $th) {
            //throw $th;
            Session::flash('err',$th->getMessage());
            return back();
        }
    }
}
