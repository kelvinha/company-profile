<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KategoriProduk;
use App\Models\Produk;
use Illuminate\Support\Facades\Validator;
use File;

class ProdukController extends Controller
{

    public function __construct()
    {
        $this->middleware('newauth');
    }

    public function index(Request $request)
    {
        $data['kategori_produk'] = KategoriProduk::all();
        $data['produk'] = Produk::where(function($query) use ($request){
            if ($request->keyword) {
                $query->where('nama', 'LIKE', '%'.$request->keyword.'%')
                        ->orWhere('harga', 'LIKE', '%'.$request->keyword.'%')
                        ->orWhere('description', 'LIKE', '%'.$request->keyword.'%');
            }

            if ($request->kategori_id_search) {
                $query->where('kategori_id', $request->kategori_id_search);
            }
        })
        ->with('getKategori')
        ->paginate(10);
        $request->flash();
        return view('backend.konten-produk.kelola-produk.index', $data);
    }

    public function store(Request $request)
    {
        // dd($request->file_produk);
        try {
            // config file
            $total_file = count($request->file_produk);
            $array_file = [];

            $validator = Validator::make($request->all(), [
                'file_produk.*' => 'mimes:jpg,jpeg,avg,png',
            ]);

            if ($validator->fails()) {
                return back()->with('err', $validator->messages()->first());
            }

            // check total file 
            if ($total_file <= 1) {
                $request->flash();
                return back()->with('err', "file produk minimal 2 file");
            }

            $produk = new Produk;
            // custom name
            $slug = strtolower(str_replace(" ","-",$request->nama));
            foreach ($request->file_produk as $key => $value) {
                $get_ext = $request->file_produk[$key]->extension(); 
                $new_name_file = $key .'-'. $slug .'.'. $get_ext;
                // dd($new_name_file);
                // end custom name
                
                // save to local folder
                $request->file_produk[$key]->move(public_path().'/files/produk/', $new_name_file); 
                array_push($array_file, $new_name_file);
            }
            
            //insert to tabel 
            $produk->nama        = ucwords($request->nama);
            $produk->harga       = $request->harga;
            $produk->filename    = json_encode($array_file);
            $produk->kategori_id = $request->kategori_id;
            $produk->description = $request->description;
            $produk->save();

            return back()->with('msg', "Produk Berhasil ditambahkan");

        } catch (\Throwable $th) {
            $request->flash();
            return back()->with('err', $th->getMessage());
        }
    }

    public function edit($id)
    {
        $data['kategori_produk'] = KategoriProduk::all();
        $data['produk'] = Produk::find($id);
        $data['index_file'] = [];
        foreach (json_decode($data['produk']->filename) as $key => $value) {
            array_push($data['index_file'], substr($value, 0, 1));
        }
        return view('backend.konten-produk.kelola-produk.edit', $data);
    }

    public function update(Request $request, $id)
    {
        // dd($request->update_produk, $request->file_produk);
        try {
            $produk     = Produk::find($id);
            $array_file = [];
            $new_array_file = [];
            $produk_array_file = [];
            // dd($produk->filename);
            $old_file = $produk->filename;
            // nambah produk
            if ($request->nama == '') {
                return back()->with('err', "nama produk tidak boleh kosong!");
            }

            if ($request->file_produk) {
                $validator = Validator::make($request->all(), [
                    'file_produk.*' => 'mimes:jpg,jpeg,avg,png',
                ]);
    
                if ($validator->fails()) {
                    return back()->with('err', $validator->messages()->first());
                }
    
                // custom name
                $slug = strtolower(str_replace(" ","-",$request->nama));
                foreach ($request->file_produk as $key => $value) {
                    $get_ext = $request->file_produk[$key]->extension(); 
                    $new_name_file = $key .'-'. $slug .'.'. $get_ext;
                    // end custom name
                    
                    // save to local folder
                    $request->file_produk[$key]->move(public_path().'/files/produk/', $new_name_file); 
                    array_push($produk_array_file, $new_name_file);
                }
            }

            // update produk
            if ($request->update_produk) {
                $slug = strtolower(str_replace(" ","-",$request->nama));
                // dd($produk->filename);
                foreach ($request->update_produk as $key => $value) {
                    // delete file
                    foreach (json_decode($produk->filename) as $value2) {
                        $index = substr($value2,0,1);
                        if ($index == $key) {
                            File::delete(public_path('files/produk/'. $value2));
                        } else {
                            array_push($array_file, $value2);
                        }
                    }

                    $get_ext = $request->update_produk[$key]->extension(); 
                    $new_name_file = $key .'-'. $slug .'.'. $get_ext;
                    $request->update_produk[$key]->move(public_path().'/files/produk/', $new_name_file); 
                    array_push($array_file, $new_name_file);

                }
                // dd($produk->filename, $array_file, $request->update_produk);
                $produk->filename = json_encode($array_file);
            }

            // new nama produk
            if ($request->nama != $produk->nama) {
                $slug = strtolower(str_replace(" ","-",$request->nama));
                foreach (json_decode($produk->filename) as $key => $value) {

                    $index = substr($value,0,2);
                    $ext = explode(".",$value);
                    $new_name_file_2 = $index . $slug . "." . $ext[1];

                    rename(public_path('/files/produk/'. $value), public_path('/files/produk/'. $new_name_file_2));
                    array_push($new_array_file, $new_name_file_2);
                }
                // dd($new_array_file, $array_file);
                $produk->filename = json_encode($new_array_file);
            }
            // dd($array_file);

            //insert to tabel 
            $produk->nama        = ucwords($request->nama);
            $produk->harga       = $request->harga;
            $produk->kategori_id = $request->kategori_id;
            $produk->description = $request->description;
            if ($request->file_produk) {
                foreach (json_decode($produk->filename) as $key => $value) {
                    array_push($produk_array_file,$value);
                }
                $produk->filename = json_encode($produk_array_file);
            }
            $produk->save();

            return redirect()->route('kelola.produk')->with('msg', "Produk Berhasil diperbarui");

        } catch (\Throwable $th) {
            return back()->with('err', $th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $produk = Produk::find($id);
            foreach (json_decode($produk->filename) as $value) {
                File::delete(public_path('files/produk/'. $value));
            }
            $produk->delete();
            return back()->with('msg', "Produk berhasil di hapus!");
        } catch (\Throwable $th) {
            return back()->with('err', $th->getMessage());
        }
    }
}
