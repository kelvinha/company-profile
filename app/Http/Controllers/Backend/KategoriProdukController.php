<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KategoriProduk;

class KategoriProdukController extends Controller
{

    public function __construct()
    {
        $this->middleware('newauth');
    }

    public function index()
    {
        $data['kategori_produk'] = KategoriProduk::paginate(5);
        return view('backend.konten-produk.kategori-produk.index', $data);
    }

    public function store(Request $request)
    {
        try {
            $nama_kategori = ucwords($request->nama_kategori);
            KategoriProduk::create([
                'nama_kategori' => $nama_kategori,
            ]);
            return back()->with('msg', "Kategori berhasil dibuat");
        } catch (\Throwable $th) {
            return back()->with('err', $th->getMessage());
        }
    }

    public function edit($id)
    {
        $data['kategori_produk'] = KategoriProduk::find($id);
        return view('backend.konten-produk.kategori-produk.edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $kategori_produk = KategoriProduk::find($id);
            $kategori_produk->update([
                'nama_kategori' => ucwords($request->nama_kategori),
            ]);
            return redirect()->route('kategori.produk')->with('msg', "Kategori produk berhasil di perbarui");
        } catch (\Throwable $th) {
            return back()->with('err', $th->getMessage());
        }
    }

    public function destroy($id)
    {
        KategoriProduk::find($id)->delete();
        return back()->with('msg', "Kategori berhasil dihapus");
    }
}
