<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Messages;

class MessagesController extends Controller
{

    public function index()
    {
        $data['messages'] = Messages::all();
        return view('backend.messages.index', $data);
    }

    public function store(Request $request)
    {   
        try {
            Messages::create($request->all());
            return redirect()->route('index');
        } catch (\Throwable $th) {
            return redirect()->route('index');
        }
    }
}
