<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KelolaKonten;
use Illuminate\Support\Facades\Validator;
use File;

class KelolaKontenController extends Controller
{
    public function __construct()
    {
        $this->middleware('newauth');
    }

    public function index(Request $request)
    {
        $data['kontens'] = KelolaKonten::where(function($query) use ($request){
            if ($request->keyword) {
                $query->where('nama', 'LIKE', '%'.$request->keyword.'%')
                        ->orWhere('type', 'LIKE', '%'.$request->keyword.'%');
            }

            if ($request->kategori_konten_search) {
                $query->where('type', $request->kategori_konten_search);
            }
        })
        ->paginate(10);
        $request->flash();
        return view('backend.kelola-konten.index', $data);
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'file' => 'mimes:jpg,jpeg,avg,png',
            ]);

            if ($validator->fails()) {
                return back()->with('err', $validator->messages()->first());
            }

            $kelola_konten = new KelolaKonten;
            // custom name
            $slug = strtolower(str_replace(" ","-",$request->nama));
            $get_ext = $request->file->extension(); 
            $new_name_file = $slug .'.'. $get_ext;
            // end custom name
            
            // save to local folder
            $request->file->move(public_path().'/files/content/', $new_name_file);  
            
            //insert to tabel 
            $kelola_konten->nama     = ucwords($request->nama);
            $kelola_konten->type     = $request->type;
            $kelola_konten->filename = $new_name_file;
            $kelola_konten->save();

            return back()->with('msg', "Konten Berhasil ditambahkan");

        } catch (\Throwable $th) {
            return back()->with('err', $th->getMessage());
        }
    }

    public function destroy($id)
    {
       try {
        $konten = KelolaKonten::find($id);
        File::delete(public_path('files/content/'. $konten->filename));
        $konten->delete();
        return back()->with('msg', "Konten Berhasil dihapuskan");
       } catch (\Throwable $th) {
        return back()->with('err', $th->getMessage());
       }
    }

    public function edit($id)
    {
        $data['konten'] = KelolaKonten::find($id);
        return view('backend.kelola-konten.edit', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            // get konten
            $konten = KelolaKonten::find($id);
            $old_path = $konten->filename;
            // cek apakah ada request file/konten jika ada, hapus yang lama
            if ($request->has('file')) {
                // validasi file
                $validator = Validator::make($request->all(), [
                    'file' => 'mimes:jpg,jpeg,avg,png',
                ]);
    
                if ($validator->fails()) {
                    return back()->with('err', $validator->messages()->first());
                }
                 // custom name
                $slug = strtolower(str_replace(" ","-",$request->nama));
                $get_ext = $request->file->extension(); 
                $new_name_file = $slug .'.'. $get_ext;
                // end custom name
                
                // save to local folder
                $request->file->move(public_path().'/files/content/', $new_name_file);  
                $konten->filename = $new_name_file;
                // delete old konten
                File::delete(public_path('files/content/'. $old_path));
            }
            
            //insert to tabel 
            $konten->nama     = ucwords($request->nama);
            $konten->type     = $request->type;
            $konten->save();


            return redirect()->route('kelola.content')->with('msg', "Konten Berhasil diperbarui");

        } catch (\Throwable $th) {
            return back()->with('err', $th->getMessage());
        }
    }
}
