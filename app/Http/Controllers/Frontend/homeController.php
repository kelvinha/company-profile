<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KelolaKonten;
use App\Models\Produk;
use App\Models\KategoriProduk;

class homeController extends Controller
{
    public function index()
    {
        $data['banner']= KelolaKonten::where('type', 'banner')->get();
        $data['portofolio']= KelolaKonten::where('type', 'portfolio')->get();
        $data['client']= KelolaKonten::where('type', 'client')->get();
        $data['partner']= KelolaKonten::where('type', 'partner')->get();

        return view('frontend.home', $data);
    }

    public function indexProduk()
    {
        $data['produk'] = Produk::get();
        $data['kategori'] = KategoriProduk::get();
        return view('frontend.product', $data);
    }

    public function indexPriceList()
    {
        return view('frontend.price-list');
    }
    public function indexVisimisi()
    {
        return view('frontend.visi-misi');
    }
    public function indexLayanan()
    {
        return view('frontend.layanan-kompentensi');
    }
    public function indexItsolution()
    {
        return view('frontend.it-solution');
    }
    public function indexPDP($nama)
    {
        $data['produk'] = Produk::with('getKategori')->where('nama',$nama)->first();
        return view('frontend.pdp', $data);
    }

}
