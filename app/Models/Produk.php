<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;
    protected $table = 'content_produk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama',
        'kategori_id',
        'harga',
        'filename',
        'description',
    ];

    public function getKategori()
    {
        return $this->belongsTo('App\Models\KategoriProduk', 'kategori_id');
    }
}
