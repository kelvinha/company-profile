<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelolaKonten extends Model
{
    use HasFactory;

    protected $table = 'content';
    protected $primaryKey = 'id';
    protected $fillable = [
        'type',
        'nama',
        'filename',
    ];
}
