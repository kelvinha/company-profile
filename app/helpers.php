<?php
function tanggalIndonesia($tgl, $jam = false)
{
    $listHari = array(1 =>'Senin', 'Selasa', 'Rabu', 'Kamis','Jumat','Sabtu,','Minggu');
    $listBulan = array(1 => 'Januari','Februari','Maret','April','Mei',	'Juni',	'Juli',	'Agustus','September','Oktober','November','Desember');
    $hari = date('N', strtotime($tgl));
    $tanggal = date('j-m-Y', strtotime($tgl));
    $pemisah = explode('-', $tanggal);
    if($jam) {
        return $newTanggal = $listHari[$hari] . ', '. $pemisah[0] . ' ' . $listBulan[intval($pemisah[1])] . ' ' . $pemisah[2] . ' ' .date("H:i",strtotime($tgl));
    }
    return $newTanggal = $listHari[$hari] . ', '. $pemisah[0] . ' ' . $listBulan[intval($pemisah[1])] . ' ' . $pemisah[2];
}
?>