-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Nov 2022 pada 17.17
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `compro`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `content`
--

CREATE TABLE `content` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `content`
--

INSERT INTO `content` (`id`, `type`, `nama`, `filename`, `created_at`, `updated_at`) VALUES
(7, 'banner', 'Ive', 'ive.jpg', '2022-11-19 05:37:02', '2022-11-19 05:37:02'),
(8, 'portfolio', 'Pertamina', 'pertamina.jpg', '2022-11-21 01:44:38', '2022-11-21 01:44:38'),
(9, 'client', 'Tes Client', 'tes-client.jpg', '2022-11-21 02:22:35', '2022-11-21 02:22:35'),
(10, 'partner', 'Tes Partner', 'tes-partner.jpg', '2022-11-21 02:45:49', '2022-11-21 02:45:49'),
(11, 'client', 'Tes Konten', 'tes-konten.jpg', '2022-11-21 07:14:38', '2022-11-21 07:14:38'),
(12, 'banner', 'Tess', 'tess.jpg', '2022-11-21 07:15:31', '2022-11-21 07:15:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `content_produk`
--

CREATE TABLE `content_produk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori_id` tinyint(4) DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `filename` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori_produk`
--

INSERT INTO `kategori_produk` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Display Banner', '2022-11-19 05:24:36', '2022-11-19 05:35:45'),
(2, 'Banner Out Door', '2022-11-19 05:26:41', '2022-11-19 05:26:41'),
(3, 'Banner In Door', '2022-11-19 05:26:54', '2022-11-19 05:26:54'),
(4, 'Digital Printing', '2022-11-19 05:27:00', '2022-11-19 05:27:00'),
(5, 'Our Client', '2022-11-20 23:48:37', '2022-11-20 23:48:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(2, 'kelvin', 'kelv@mail.com', 'kelvi', '<p>Lorem ipsum dolor sit amet. Aut autem ipsum et saepe ratione est fugiat tempora vel magnam porro ut quam sapiente est quisquam repellendus! Et atque officia et iste Quis sed autem dicta ut magnam aperiam et voluptatibus delectus qui omnis internos ea vero amet. Sed rerum cupiditate ad magni vero non quasi sunt qui consequatur nobis in facilis recusandae. </p><p>Et sint facilis eum consequatur magnam sed alias voluptatibus cum magnam ullam qui expedita distinctio ad unde impedit cum soluta illo. Est eveniet corporis qui ipsum atque qui voluptas distinctio! </p><p>Aut expedita alias eum corporis perferendis sed illo autem eos veritatis magni sed quisquam consequatur hic nihil aperiam? Et reprehenderit sint aut perspiciatis ipsam At facilis libero? </p>\n<p>Lorem ipsum dolor sit amet. Aut autem ipsum et saepe ratione est fugiat tempora vel magnam porro ut quam sapiente est quisquam repellendus! Et atque officia et iste Quis sed autem dicta ut magnam aperiam et voluptatibus delectus qui omnis internos ea vero amet. Sed rerum cupiditate ad magni vero non quasi sunt qui consequatur nobis in facilis recusandae. </p><p>Et sint facilis eum consequatur magnam sed alias voluptatibus cum magnam ullam qui expedita distinctio ad unde impedit cum soluta illo. Est eveniet corporis qui ipsum atque qui voluptas distinctio! </p><p>Aut expedita alias eum corporis perferendis sed illo autem eos veritatis magni sed quisquam consequatur hic nihil aperiam? Et reprehenderit sint aut perspiciatis ipsam At facilis libero? </p>\n<p>Lorem ipsum dolor sit amet. Aut autem ipsum et saepe ratione est fugiat tempora vel magnam porro ut quam sapiente est quisquam repellendus! Et atque officia et iste Quis sed autem dicta ut magnam aperiam et voluptatibus delectus qui omnis internos ea vero amet. Sed rerum cupiditate ad magni vero non quasi sunt qui consequatur nobis in facilis recusandae. </p><p>Et sint facilis eum consequatur magnam sed alias voluptatibus cum magnam ullam qui expedita distinctio ad unde impedit cum soluta illo. Est eveniet corporis qui ipsum atque qui voluptas distinctio! </p><p>Aut expedita alias eum corporis perferendis sed illo autem eos veritatis magni sed quisquam consequatur hic nihil aperiam? Et reprehenderit sint aut perspiciatis ipsam At facilis libero? </p>\n<p>Lorem ipsum dolor sit amet. Aut autem ipsum et saepe ratione est fugiat tempora vel magnam porro ut quam sapiente est quisquam repellendus! Et atque officia et iste Quis sed autem dicta ut magnam aperiam et voluptatibus delectus qui omnis internos ea vero amet. Sed rerum cupiditate ad magni vero non quasi sunt qui consequatur nobis in facilis recusandae. </p><p>Et sint facilis eum consequatur magnam sed alias voluptatibus cum magnam ullam qui expedita distinctio ad unde impedit cum soluta illo. Est eveniet corporis qui ipsum atque qui voluptas distinctio! </p><p>Aut expedita alias eum corporis perferendis sed illo autem eos veritatis magni sed quisquam consequatur hic nihil aperiam? Et reprehenderit sint aut perspiciatis ipsam At facilis libero? </p>\n', '2022-11-20 03:38:49', '2022-11-20 03:38:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_11_12_150511_create_kategori_content_table', 2),
(6, '2022_11_12_150925_create_content_table', 2),
(7, '2022_11_12_151014_create_content_produk_table', 2),
(8, '2022_11_19_123941_create_messages_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` int(11) NOT NULL DEFAULT 1,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `roles`, `username`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ganang', 1, 'admin', NULL, '$2y$10$rVuV6Wzpoq7Qp5sjwUAc0ufPBOzxg67tOgDkBYy77aXTEFZafPS/.', NULL, '2022-11-10 06:38:34', '2022-11-10 06:38:34');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `content_produk`
--
ALTER TABLE `content_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `content`
--
ALTER TABLE `content`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `content_produk`
--
ALTER TABLE `content_produk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
