<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.home');
// });

Route::get('/', [App\Http\Controllers\Frontend\homeController::class, 'index'])->name('index');
Route::get('/digital-printing', [App\Http\Controllers\Frontend\homeController::class, 'indexProduk'])->name('index.produk');
Route::get('/price-list', [App\Http\Controllers\Frontend\homeController::class, 'indexPriceList'])->name('index.price.list');
Route::get('/visi-misi', [App\Http\Controllers\Frontend\homeController::class, 'indexVisimisi'])->name('index.visimisi');
Route::get('/layanan-kompentensi', [App\Http\Controllers\Frontend\homeController::class, 'indexLayanan'])->name('index.layanan');
Route::get('/it-solution', [App\Http\Controllers\Frontend\homeController::class, 'indexItsolution'])->name('index.itsolution');
Route::get('/product-detail-page', [App\Http\Controllers\Frontend\homeController::class, 'indexPDP'])->name('index.pdp');



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::prefix('admin')->group(function() {
    // dashboard
    Route::get('/dashboard', [App\Http\Controllers\Backend\DashboardController::class, 'index'])->name('dashboard');
    // kelola content
    Route::get('/kelola-content', [App\Http\Controllers\Backend\KelolaKontenController::class, 'index'])->name('kelola.content');
    Route::post('/kelola-content/store', [App\Http\Controllers\Backend\KelolaKontenController::class, 'store'])->name('kelola.content.store');
    Route::post('/kelola-content/delete/{id}', [App\Http\Controllers\Backend\KelolaKontenController::class, 'destroy'])->name('kelola.content.delete');
    Route::get('/kelola-content/edit/{id}', [App\Http\Controllers\Backend\KelolaKontenController::class, 'edit'])->name('kelola.content.edit');
    Route::post('/kelola-content/update/{id}', [App\Http\Controllers\Backend\KelolaKontenController::class, 'update'])->name('kelola.content.update');
    // kategori produk
    Route::prefix('/produk')->group(function() {
        // crud kategori
        Route::get('/kategori-produk', [App\Http\Controllers\Backend\KategoriProdukController::class, 'index'])->name('kategori.produk');
        Route::post('/kategori-produk/store', [App\Http\Controllers\Backend\KategoriProdukController::class, 'store'])->name('kategori.produk.store');
        Route::get('/kategori-produk/edit/{id}', [App\Http\Controllers\Backend\KategoriProdukController::class, 'edit'])->name('kategori.produk.edit');
        Route::post('/kategori-produk/delete/{id}', [App\Http\Controllers\Backend\KategoriProdukController::class, 'destroy'])->name('kategori.produk.delete');
        Route::post('/kategori-produk/update/{id}', [App\Http\Controllers\Backend\KategoriProdukController::class, 'update'])->name('kategori.produk.update');
        // crud produk
        Route::get('/kelola-produk', [App\Http\Controllers\Backend\ProdukController::class, 'index'])->name('kelola.produk');
        Route::post('/kelola-produk/store', [App\Http\Controllers\Backend\ProdukController::class, 'store'])->name('kelola.produk.store');
        Route::get('/kelola-produk/edit/{id}', [App\Http\Controllers\Backend\ProdukController::class, 'edit'])->name('kelola.produk.edit');
        Route::post('/kelola-produk/delete/{id}', [App\Http\Controllers\Backend\ProdukController::class, 'destroy'])->name('kelola.produk.delete');
        Route::post('/kelola-produk/update/{id}', [App\Http\Controllers\Backend\ProdukController::class, 'update'])->name('kelola.produk.update');
    });

    // messages
    Route::get('/messages', [App\Http\Controllers\Backend\MessagesController::class, 'index'])->name('messages')->middleware('newauth');
    Route::post('/messages/store', [App\Http\Controllers\Backend\MessagesController::class, 'store'])->name('messages.store');
});
